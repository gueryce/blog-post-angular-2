import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Post} from '../models/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  // posts: Post[] = [];
  posts: Post[] = [
    {
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in euismod justo.',
      title: 'Mon premier post',
      loveIts: 0,
      created : new Date('2008/05/10 12:08:20')
    },
    {
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in euismod justo.',
      title: 'Mon deuxième post',
      loveIts: 0,
      created : new Date('2008/05/11 12:08:20')
    },
    {
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in euismod justo.',
      title: 'Encore un post',
      loveIts: 0,
      created : new Date('2008/05/12 12:08:20')
    }
  ];

  postsSubject = new Subject<Post[]>();

  constructor() {
    this.getPosts();
    console.log('service ' + this);
    console.log('service ' + this.posts[0].loveIts);
  }

  emitPosts() {
    this.postsSubject.next(this.posts);
  }

  getPosts(){
    this.emitPosts();
  }

  createNewPost(newPost: Post) {
    this.posts.push(newPost);
    this.emitPosts();
  }

  removePost(post: Post) {
    const postIndexToRemove = this.posts.findIndex(
      value => {
        if(value === post) {
          return true;
        }
      }
    );
    this.posts.splice(postIndexToRemove,1);
    this.emitPosts();
  }

  savePost(post: Post) {
    const postIndex = this.posts.findIndex(
      value => {
        if(value === post) {
          return true;
        }
      }
    );
    this.posts[postIndex] = post;
    // Pour verifier que le post a bien été sauvegardé
    console.log(this.posts[postIndex].loveIts);
    this.emitPosts();
  }
}
