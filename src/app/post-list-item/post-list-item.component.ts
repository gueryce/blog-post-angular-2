import {Component, Input, OnInit} from '@angular/core';
import {Post} from '../models/post.model';
import {Subscription} from 'rxjs';
import {PostsService} from '../services/posts.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() post: Post;

  constructor(private postsService: PostsService, private router: Router) {}

  ngOnInit() {

  }

  countChange(event)  {
    console.log(event);
    this.post.loveIts = event;
    this.postsService.savePost(this.post);
  }



}
