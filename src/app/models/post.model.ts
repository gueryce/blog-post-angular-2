export class Post {
  created: Date;
  loveIts: number;

  constructor(public title: string, public content: string) {
    this.loveIts = 0;
    this.created = new Date();
  }
}
