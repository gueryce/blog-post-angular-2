import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {isNumber} from 'util';
import {Post} from '../models/post.model';
import {PostsService} from '../services/posts.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  @Output() postLoveItEvent: EventEmitter<number> = new EventEmitter<number>();
  @Input() post: Post;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
  }

  loveIt() {
    console.log('Add love');
    this.post.loveIts ++;
    this.postLoveItEvent.emit(this.post.loveIts);
  }

  dontLoveIt() {
    console.log('Remove love');
    this.post.loveIts --;
    this.postLoveItEvent.emit(this.post.loveIts);
  }

  onDeletePost(post: Post) {
    this.postsService.removePost(post);
  }

}
